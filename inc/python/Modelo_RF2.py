# -*- coding: utf-8 -*-
"""
Created on Mon Nov  9 21:29:55 2020

@author: byron.otalvaro
"""
import pandas as pandas
import base64

import json
import pandas as pd
import numpy as np
import joblib
import statsmodels.formula.api as smf
import sys
import os
#print ("Número de parámetros: ", len(sys.argv))
#print ("Lista de argumentos: ", sys.argv)

def predict():
	Input_Pantalla = json.loads(sys.argv[1])
	# {
	#	"genero":"MUJER",
	#	"rangoedad":"1",
	#	"categoria_afiliado":"B",
	#	"grupo_etnico":"PALANQUERO",
	#	"ubicacion_residencia":"RURAL",
	#	"conflicto":1,
	#	"desplazamiento":0,
	#	"discapacidad":1,
	#	"caja_name":"COMFACOR",
	#	"region":"Región Centro Oriente"
	#}
	Input_Pantalla['region'] = Input_Pantalla['region'].replace(' ','_')
	Input_Pantalla['region'] = Input_Pantalla['region'].replace('region_Región_Eje_Cafetero_-_Antioquia','region_Región_Eje_Cafetero_Antioquia')
	DicEntrada = {};
	DicEntrada["rangoedad"] = Input_Pantalla["rangoedad"]

	DicEntrada["conflicto"]=Input_Pantalla["conflicto"]
	DicEntrada["desplazamiento"]=Input_Pantalla["desplazamiento"]
	DicEntrada["discapacidad"]=Input_Pantalla["discapacidad"]

	#categoria_afiliado
	if (Input_Pantalla["categoria_afiliado"]=='A'):
	    DicEntrada["categoria_afiliado_B"]= 0
	    DicEntrada["categoria_afiliado_C"]= 0
	    DicEntrada["categoria_afiliado_D"]=0

	if (Input_Pantalla["categoria_afiliado"]=='B'):
	    DicEntrada["categoria_afiliado_B"]= 1
	    DicEntrada["categoria_afiliado_C"]= 0
	    DicEntrada["categoria_afiliado_D"]=0

	if (Input_Pantalla["categoria_afiliado"]=='C'):
	    DicEntrada["categoria_afiliado_B"]= 0
	    DicEntrada["categoria_afiliado_C"]= C
	    DicEntrada["categoria_afiliado_D"]=0

	if (Input_Pantalla["categoria_afiliado"]=='D'):
	    DicEntrada["categoria_afiliado_B"]= 0
	    DicEntrada["categoria_afiliado_C"]= 0
	    DicEntrada["categoria_afiliado_D"]=D

	#Residencia
	DicEntrada["ubicacion_residencia_URBANA"]=0
	if Input_Pantalla["ubicacion_residencia"] == "RURAL":
	    DicEntrada["ubicacion_residencia_URBANA"]=1

	#Region
	DicEntrada["region_Región_Centro_Oriente"]=0
	DicEntrada["region_Región_Centro_Sur"]=0
	DicEntrada["region_Región_Eje_Cafetero_Antioquia"]=0
	DicEntrada["region_Región_Llano"]=0
	DicEntrada["region_Región_Pacífico"]=0
	if Input_Pantalla["region"] != "Región Caribe":
	    DicEntrada["region_" + Input_Pantalla["region"]]=1

	DicEntrada["caja_name_CAFAM"]=0
	DicEntrada["caja_name_CAFAMAZ"]=0
	DicEntrada["caja_name_CAFASUR"]=0
	DicEntrada["caja_name_CAJACOPI_Barranquilla"]=0
	DicEntrada["caja_name_CAJAMAG"]=0
	DicEntrada["caja_name_CAJASAI"]=0
	DicEntrada["caja_name_CAJASAN"]=0
	DicEntrada["caja_name_COFREM"]=0
	DicEntrada["caja_name_COLSUBSIDIO"]=0
	DicEntrada["caja_name_COMBARRANQUILLA"]=0
	DicEntrada["caja_name_COMCAJA"]=0
	DicEntrada["caja_name_COMFABOY"]=0
	DicEntrada["caja_name_COMFACA_Comfamiliar_Caqueta"]=0
	DicEntrada["caja_name_COMFACASANARE"]=0
	DicEntrada["caja_name_COMFACAUCA"]=0
	DicEntrada["caja_name_COMFACESAR"]=0
	DicEntrada["caja_name_COMFACHOCO"]=0
	DicEntrada["caja_name_COMFACOR"]=0
	DicEntrada["caja_name_COMFACUNDI"]=0
	DicEntrada["caja_name_COMFAGUAJIRA"]=0
	DicEntrada["caja_name_COMFAMA"]=0
	DicEntrada["caja_name_COMFAMILIAR_Atlántico"]=0
	DicEntrada["caja_name_COMFAMILIAR_CAMACOL"]=0
	DicEntrada["caja_name_COMFAMILIAR_Caldas"]=0
	DicEntrada["caja_name_COMFAMILIAR_Huila"]=0
	DicEntrada["caja_name_COMFAMILIAR_Nariño"]=0
	DicEntrada["caja_name_COMFAMILIAR_Putumayo"]=0
	DicEntrada["caja_name_COMFAMILIAR_Risaralda"]=0
	DicEntrada["caja_name_COMFAMILIAR_de_Cartagena_y_Bolivar_"]=0
	DicEntrada["caja_name_COMFANDI"]=0
	DicEntrada["caja_name_COMFANORTE"]=0
	DicEntrada["caja_name_COMFAORIENTE"]=0
	DicEntrada["caja_name_COMFASUCRE"]=0
	DicEntrada["caja_name_COMFATOLIMA"]=0
	DicEntrada["caja_name_COMFENALCO_Antioquia"]=0
	DicEntrada["caja_name_COMFENALCO_Cartagena"]=0
	DicEntrada["caja_name_COMFENALCO_Quindio"]=0
	DicEntrada["caja_name_COMFENALCO_Santander"]=0
	DicEntrada["caja_name_COMFENALCO_Tolima"]=0
	DicEntrada["caja_name_COMFENALCO_Valle_del_Cauca"]=0
	DicEntrada["caja_name_COMFIAR_Arauca"]=0
	DicEntrada["caja_name_COMPENSAR"]=0

	if Input_Pantalla["caja_name"] != "CAFABA":
	    DicEntrada["caja_name_" + Input_Pantalla["caja_name"]]=1

	DicEntrada["genero_INDETERMINADO"] = 0
	DicEntrada["genero_MUJER"] = 0
	if Input_Pantalla["genero"] != "HOMBRE":
	    DicEntrada["genero_" + Input_Pantalla["genero"]] = 1

	#Grupo étcnico
	#DicEntrada["grupo_etnico_AFROCOLOMBIANO"]=0
	DicEntrada["grupo_etnico_COMUNIDAD NEGRA"]=0
	DicEntrada["grupo_etnico_COMUNIDAD RAIZAL"]=0
	DicEntrada["grupo_etnico_INDÃ�GENA"]=0
	DicEntrada["grupo_etnico_NINGUNO DE LOS ANTERIORES"]=0
	DicEntrada["grupo_etnico_NO DISPONIBLE"]=0
	DicEntrada["grupo_etnico_PALANQUERO"]=0
	DicEntrada["grupo_etnico_ROOM/GITANO"]=0

	if Input_Pantalla["grupo_etnico"] != "AFROCOLOMBIANO":
	    DicEntrada["grupo_etnico_" + Input_Pantalla["grupo_etnico"]]=1

	# 2. Generar el dataframe  de entrada al modelo
	filename = '/var/www/html/wp-content/plugins/ssf_ds4a/inc/python/Modelo_RF_aprobacion.sav'
	loaded_model = joblib.load(filename)
	model1 = pd.DataFrame(DicEntrada, index=[0])
	result = loaded_model.predict(model1)
	print(result[0])

predict()

