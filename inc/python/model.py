# -*- coding: utf-8 -*-
"""
Created on Mon Nov  9 21:29:55 2020

@author: byron.otalvaro
"""
import pandas as pandas
import base64

import json
import pandas as pd
import numpy as np
import joblib
import statsmodels.formula.api as smf
import sys
import os 
#print ("Número de parámetros: ", len(sys.argv))
#print ("Lista de argumentos: ", sys.argv)

def predict():
    #Get the BODY input data from request call
    model_file = '/var/www/html/wp-content/plugins/ssf_ds4a/inc/python/finalized_model.sav'
    model = joblib.load(model_file)
    data = {
                "Edad":"2",
                "Sexo":"M",
                "Categoria":"B",
                "Nro Beneficios liquidados":3,
                "Municipio":2524,
                "Zona":1,
                "Desplazado":0,
                "Victima conflicto":1,
                "Discapacitado":0,
                "Grupo Étnico":0,
                "Vulnerabilidad":1
                }
    #Transform the input data to dataframe using pandas
    test = pd.DataFrame(data, index=[0])

    #PAra pruebas postman para poder ejecutar el modelo
    # 3*X1 + 2*X2+ 1
    test ["x1"] = 3 #test.Edad
    test ["x2"] = 4 #test.Edad


    test = test.astype({"x1": int, "x2": int})

    prediction = model.predict(test)

    message ={
        "status":200,
        "task": "Result of prediction",
        "message": str(prediction[0])
    }
#    response = jsonify(message)
#    response.status_code  = 200

    print(message)

predict()

