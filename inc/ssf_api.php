<?php
//error_reporting(E_ALL);
//ini_set('display_errors', '1');
class DS4A_93_Controller {
 
    // Here initialize our namespace and resource name.
    public function __construct() {
        $this->namespace     = '/DS4A_93/v1';
        $this->resource_name = 'api';
    }
 
    // Register our routes.
    public function register_routes() {
	register_rest_route( $this->namespace, '/' . $this->resource_name . '/summary/(?P<id>[a-zA-Z0-9-]+)', array(
            array(
                'methods'   => 'GET',
                'callback'  => array( $this, 'get_summary' )
            )
        ) );

	register_rest_route( $this->namespace, '/' . $this->resource_name . '/summarydatabydeptomunicipio', array(
            array(
                'methods'   => 'GET',
                'callback'  => array( $this, 'get_summarydatabydeptomunicipio' )
            )
	) );

	register_rest_route( $this->namespace, '/' . $this->resource_name . '/summaryvulnerablebydeptomunicipio', array(
            array(
                'methods'   => 'GET',
                'callback'  => array( $this, 'get_summaryvulnerablebydeptomunicipio' )
            )
        ) );

	register_rest_route( $this->namespace, '/' . $this->resource_name . '/model', array(
            array(
	                'methods'   => 'POST',
			'args' => array(
	     			'rangoedad' => array('required' => true,'type' => 'integer')
				,'conflicto' => array('required' => true,'type' => 'boolean')
				,'desplazamiento' => array('required' => true,'type' => 'boolean')
				,'discapacidad' => array('required' => true,'type' => 'boolean')
				,'categoria_afiliado' => array('required' => true,'type' => 'string', 'enum' => array('A','B','C','D'))
				,'ubicacion_residencia' => array('required' => true,'type' => 'string', 'enum' => array('RURAL','URBANA'))
				,'region' => array('required' => true,'type' => 'string', 'enum' => array('Región Caribe','Región Centro Sur','Región Eje Cafetero - Antioquia','Región Centro Oriente','Región Pacífico','Región Llano'))
				,'caja_name' => array('required' => true,'type' => 'string', 'enum' => array("COMFAMILIAR CAMACOL","COMFENALCO Antioquia","COMFAMA","CAJACOPI Barranquilla","COMBARRANQUILLA","COMFAMILIAR Atlántico","COMFENALCO Cartagena","COMFAMILIAR de Cartagena y Bolivar ","COMFABOY","COMFAMILIAR Caldas","COMFACA Comfamiliar Caqueta","COMFACAUCA","COMFACESAR","COMFACOR","CAFAM","COLSUBSIDIO","COMPENSAR","COMFACUNDI","COMFACHOCO","COMFAGUAJIRA","COMFAMILIAR Huila","CAJAMAG","COFREM","COMFAMILIAR Nariño","COMFAORIENTE","COMFANORTE","CAFABA","CAJASAN","COMFENALCO Santander","COMFASUCRE","COMFENALCO Quindio","COMFAMILIAR Risaralda","CAFASUR","COMFATOLIMA","COMFENALCO Tolima","COMFENALCO Valle del Cauca","COMFANDI","COMFAMILIAR Putumayo","CAJASAI","CAFAMAZ","COMFIAR Arauca","COMCAJA","COMFACASANARE")) 
				,'genero' => array('required' => true,'type' => 'string', 'enum' => array('HOMBRE','MUJER','INDETERMINADO'))
				,'grupo_etnico' => array('required' => true,'type' => 'string', 'enum' => array('NINGUNO DE LOS ANTERIORES','AFROCOLOMBIANO','COMUNIDAD NEGRA','COMUNIDAD RAIZAL','INDÃ�GENA','PALANQUERO','ROOM/GITANO'))
			),
	        	'callback'  => array( $this, 'get_modelprediction' )
		)
        ) );
    }

    /**
     * Check permissions for the posts.
     *
     * @param WP_REST_Request $request Current request.
     */
    public function get_items_permissions_check( $request ) {
        if ( ! current_user_can( 'read' ) ) {
            return new WP_Error( 'rest_forbidden', esc_html__( 'You cannot view the post resource.' ), array( 'status' => $this->authorization_status_code() ) );
        }
        return true;
    }

    public function get_summary( $request ) {
	$param = $request->get_param( 'id' );
	$sql = '';
        switch($param){
		case 'approvaldays': $sql = 'select * from dwh.approvaldays';break;
		case 'approvalsbyfilingdate': $sql = 'select * from dwh.approvalsbyfilingdate order by 1,2';break;
               	case 'summarydata': $sql = 'select * from dwh.summarydata';break;
               	case 'summarydatabymunicipio': $sql = 'select * from dwh.summarydatabymunicipio order by municipio';break;
               	case 'summarydatabydepto': $sql = 'select * from dwh.summarydatabydepto order by departamento';break;
               	case 'summarydatabygender': $sql = 'select * from dwh.summarydatabygender order by 2,1';break;
               	case 'summarydatabycategory': $sql = 'select * from dwh.summarydatabycategory order by 2,1';break;
		case 'summarybyvulnerablepopulation': $sql = 'select * from dwh.summarybyvulnerablepopulation';break;
		case 'summaryvulnerablepopulationbyfilingdate': $sql = "select *, 'benefits' status from dwh.summaryvulnerablepopulationbyfilingdate order by 2";break;
		case 'summarybyvulnerablebycaja': $sql = "select * from dwh.summarybyvulnerablebycaja order by 1";break;
		case 'summarybyvulnerablesuspended': $sql = "select * from dwh.summarybyvulnerablesuspended";break;
	}
        return $this->get_data($sql);
    }

    public function get_summarydatabydeptomunicipio( $request ) {
	$depto = 'select * from dwh.summarydatabydepto order by departamento';
    	$municipio = 'select * from dwh.summarydatabymunicipio order by municipio';
	return array("summarybydepto" => $this->get_data($depto), "summarybymunicipio" => $this->get_data($municipio));
    }

    public function get_summaryvulnerablebydeptomunicipio( $request ) {
        $depto = 'select * from dwh.summaryvulnerablebydepto order by departamento';
        $municipio = 'select * from dwh.summaryvulnerablebymunicipio order by municipio';
        return array("summarybydepto" => $this->get_data($depto), "summarybymunicipio" => $this->get_data($municipio));
    }

    public function get_modelprediction( $request ) {
	$params = $request->get_params();
	foreach($params as $param => $value){
		if($value === true)
			$params[$param]=1;
		if($value === false)
			$params[$param]=0;
	}
	$params = json_encode($params);
	$command = "python3  -W ignore ".plugin_dir_path(__FILE__)."/python/Modelo_RF2.py '".$params."'";
	$output = exec($command);
	$json = new stdClass();
	$json->data = array("modelresult"=>(int)$output);
	return $json;
    }

    public function get_data($sql){
	global $wpdb;
	$result = $wpdb->get_results( $wpdb->prepare( $sql) );
	$json = new stdClass();
	$json->data = $result;
	return $json;
    }
    /**
     *
     * @param WP_REST_Request $request Current request.
     */
    public function get_item_permissions_check( $request ) {
        if ( ! current_user_can( 'read' ) ) {
            return new WP_Error( 'rest_forbidden', esc_html__( 'You cannot view this resource.' ), array( 'status' => $this->authorization_status_code() ) );
        }
        return true;
    }
 
    // Sets up the proper HTTP status code for authorization.
    public function authorization_status_code() {
 
        $status = 401;
 
        if ( is_user_logged_in() ) {
            $status = 403;
        }
 
        return $status;
    }
}
 

