<?php
/* @package ssf_ds4a
 * Plugin Name: SSF DS4A
 * Description: Plugin for SSF by 93 Group of DS4A 
 * Plugin URI: http://www.byronotalvaro.com
 * Author: Byron Otalvaro
 * Version: 1.0
 * License: GPLv2
 */
//error_reporting(E_ALL);
//ini_set('display_errors', '1');

require_once plugin_dir_path(__DIR__)."/ssf_ds4a/inc/ssf_api.php";

 
// Function to register our new routes from the controller.
function ds4a_register_api_routes() {
    $controller = new DS4A_93_Controller();
    $controller->register_routes();
}
 
add_action( 'rest_api_init', 'ds4a_register_api_routes' );


function ssf_ds4a_dictionary($lang){
	$dictionary = array(
				"en_US" => array("card_general_1_title"=>"Approved Benefits"
						,"card_general_2_title"=>"Not Approved Benefits"
						,"card_general_3_title"=>"Average Approval Days"
						,"card_general_4_title"=>"Suspended Benefits"
						,"ds_general_timeseries_title"=>"Benefits Status by Filing Date"
						,"ds_general_map_title"=>"Geography distribution of the benefits request"
						,"ds_general_genre_title"=>"Benefits by gender"
						,"ds_general_cat_title"=>"Benefits by Affiliation Category"
						,"approved"=>"Approved"
						,"notapproved"=>"Not Approved"
						,"avgdays"=>"Avg Days"
						,"numberofbenefits"=>"Number of benefits"
						,"card_vulnerable_1_title"=>"Handicapped Population"
                                                ,"card_vulnerable_2_title"=>"Displaced Population"
                                                ,"card_vulnerable_3_title"=>"Victims of the Conflict"
                                                ,"card_vulnerable_4_title"=>"% Vulnerable Population"
						,"benefits"=>"Benefits"
						,"ds_vulnerable_timeseries_title"=>"Benefits approved to vulnerable population per day"
						,"ds_vulnerable_table_title"=>'Benefits approved to vulnerable population by "Caja"'
						,"caja_name"=>"Caja de compensación"
                                                ,"sum_discapacidad"=>"Handicapped"
		                                ,"sum_desplazamiento"=>"Displaced"
                                                ,"sum_conflicto"=>"On the Conflict"
						,"vulnerable"=>"Vulnerable"
						,"novulnerable"=>"No Vulnerable"
						,"ds_vulnerable_pie_title"=>"Vulnerable vs. suspended non-vulnerable population"
						,"ds_vulnerable_map_title"=>"Vulnerable Population by Region"
						, "genero" => "Gender"
						, "hombre" => "Man"
						, "mujer" => "Woman"
						, "indeterminado" => "Indeterminate"
						, "rangoedad" => "Age range"
						, "grupo_etnico" => "Ethnic group"
						, "region" => "Region"
						, "ubicacion_residencia" => "Residence Location"
						, "RURAL" => "Rural"
						, "URBANA" => "Urban"
						, "factor_vulnerabilidad" => "Factor of vulnerability"
						, "categoria_afiliado" => "Affiliate category"
						, "menorde" => "Less than"
						, "mayorde"=>"Over"
						, "anios" => "Years Old"
						, "de" => "From"
						, "a" => "To"
						, "NINGUNO" => "None"
						, "AFROCOLOMBIANO" => "Afro-Colombian"
						, "COMUNIDAD NEGRA" => "Black community"
						, "COMUNIDAD RAIZAL" => "Raizal Community"
						, "INDIGENA" => "Indigenous"
						, "PALANQUERO" => "Palenquero"
						, "ROOM/GITANO" => "Room / Gitano"
						, "Región Caribe" => "Caribbean Region"
						, "Región Centro Sur" => "South Central Region"
						, "Región Eje Cafetero - Antioquia" => "Eje Cafetero Region - Antioquia"
						, "Región Centro Oriente" => "Central East Region"
						, "Región Pacífico" => "Pacific Region"
						, "Región Llano" => "Plain Region"
						, "ninguno" => "None"
						, "conflicto" => "Armed conflict"
						, "desplazamiento" => "Displacement"
						, "discapacidad" => "Handicapped"
						,"Submit"=>"Submit"
						,"ds_model_legend"=>"After having collected information from the compensation funds of more than 400 thousand requests to obtain the economic benefit, decreed by the national government under Legislative Decree 488 of March 27, 2020, a mathematical and statistical model was achieved that will help you. Fill the form and click in Submit bottom to get the result, please"
						,"modeltitle"=>"What's it for?"
						,"requestapproved"=>"Your request may be approved"
						,"requestnotapproved"=>"Your request may not be approved"),
				"es_CO" => array("card_general_1_title"=>"Beneficios aprobados"
						,"card_general_2_title"=>"Beneficios no aprobados"
                                                ,"card_general_3_title"=>"Días promedio de aprobación"
						,"card_general_4_title"=>"Beneficios suspendidos"
						,"ds_general_timeseries_title"=>"Estado de los Beneficios por fecha de radicación"
						,"ds_general_map_title"=>"Distribución geográfica de la solicitud de beneficios"
						,"ds_general_genre_title"=>"Beneficios por género"
						,"ds_general_cat_title"=>"Beneficios por Categoría de afiliación"
						,"approved"=>"Aprobados"
                                                ,"notapproved"=>"No aprobados"
						,"avgdays"=>"Días promedio"
						,"numberofbenefits"=>"Número de beneficios"
						,"card_vulnerable_1_title"=>"Población en discapacidad"
                                                ,"card_vulnerable_2_title"=>"Población desplazada"
						,"card_vulnerable_3_title"=>"Víctimas del conflicto"
                                                ,"card_vulnerable_4_title"=>"% Población vulnerable"
						,"benefits"=>"Beneficios"
						,"ds_vulnerable_timeseries_title"=>"Beneficios aprobados a población vulnerable por día"
						,"ds_vulnerable_table_title"=>"Beneficios aprobados a población vulnerable por Caja"
						,"caja_name"=>"Caja de compensación"
						,"sum_discapacidad"=>"Discapacidad"
						,"sum_desplazamiento"=>"Despalazada"
						,"sum_conflicto"=>"Conflicto"
						,"vulnerable"=>"Vulnerable"
                                                ,"novulnerable"=>"No Vulnerable"
						,"ds_vulnerable_pie_title"=>"Vulnerable suspendida vs no vulnerable suspendida"
						,"ds_vulnerable_map_title"=>"Población vulnerable por regiones"
						,"genero"=>"Género"
						,"hombre"=>"Hombre"
						,"mujer"=>"Mujer"
						,"indeterminado"=>"Indeterminado"
						,"rangoedad"=>"Rango de edad"
						,"grupo_etnico"=>"Grupo étnico"
						,"region"=>"Región"
						,"ubicacion_residencia"=>"Ubicación residencia"
						,"RURAL"=>"Rural"
						,"URBANA"=>"Urbana"
						,"factor_vulnerabilidad"=>"Factor de vulnerabilidad"
						,"categoria_afiliado"=>"Categoría del afiliado"
						,"menorde"=>"Menor de"
						,"mayorde"=>"Mayor de"
						,"anios"=>"Años"
						,"de"=>"De"
						,"a"=>"a"
						,"NINGUNO"=>"Ninguno"
						,"AFROCOLOMBIANO"=>"Afrocolombiano"
						,"COMUNIDAD NEGRA"=>"Comunidad negra"
						,"COMUNIDAD RAIZAL"=>"Comunidad raizal"
						,"INDIGENA"=>"Indígena"
						,"PALANQUERO"=>"Palenquero"
						,"ROOM/GITANO"=>"Room/Gitano"
						,"Región Caribe"=>"Región Caribe"
						,"Región Centro Sur"=>"Región Centro Sur"
						,"Región Eje Cafetero - Antioquia"=>"Región Eje Cafetero - Antioquia"
						,"Región Centro Oriente"=>"Región Centro Oriente"
						,"Región Pacífico"=>"Región Pacífico"
						,"Región Llano"=>"Región Llano"
						,"ninguno"=>"Ninguno"
						,"conflicto"=>"Conflicto armado"
						,"desplazamiento"=>"Desplazamiento"
						,"discapacidad"=>"Discapacidad"
						,"Submit"=>"Enviar"
						,"ds_model_legend"=>"Despues de haber recopilado información de las cajas de compensación de mas de 400 mil solicitudes para obtener el beneficio económico, decretado por el gobierno nacional bajo Decreto Legislativo 488 del 27 de marzo de 2020, se logro realizar un modelo matemático y estadístico que le ayudará a verificar si su solicitud puede llegar a ser aceptada o no. Por favor llene el formulario y haga clic en el botón enviar para obtener el resultado"
						,"modeltitle"=>"¿Para qué es esto?"
						,"requestapproved"=>"Su solicitud podría ser aprobada"
						,"requestnotapproved"=>"Su solicitud podría no ser aprobada")
			);
	return $dictionary[$lang];
}


function load_scripts(){
    	global $post;
    	$post_slug=$post->post_name;
	$pages = array("dashboard","dashboard-2","poblacion-vulnerable-beneficiada","vulnerable-population-benefited","evalue-solicitud","evaluate-your-application","evalue-solicitud"
			,"evaluate-your-application","api","api-2");
	if(in_array($post_slug, $pages)){
	     	wp_register_style("datatables_css","https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css", false, "1.10.22");
	       	wp_enqueue_style("datatables_css");

		wp_register_style("bootstrap_css","https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css", false, "4.4.1");
	       	wp_enqueue_style("bootstrap_css");

		wp_register_style("fontawesome_css",plugin_dir_url(__FILE__)."css/font-awesome/css/font-awesome.min.css", false, "4.7.0");
	        wp_enqueue_style("fontawesome_css");

//		wp_register_style("ssf_ds4a_css",plugin_dir_url(__FILE__)."css/style.css", false, "1.0.0");
//	        wp_enqueue_style("ssf_ds4a_css");

	       	wp_enqueue_script("jquery", "https://code.jquery.com/jquery-3.5.1.js");
	        wp_enqueue_script("datatables_js", "https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js", array('jquery'));
		wp_enqueue_script("highcharts_maps_js", "https://code.highcharts.com/maps/highmaps.js");
		wp_enqueue_script("highcharts_data_js", "https://code.highcharts.com/modules/data.js", array('highcharts_maps_js'));
		wp_enqueue_script("highcharts_proj4_js", "https://cdnjs.cloudflare.com/ajax/libs/proj4js/2.3.6/proj4.js", array('highcharts_maps_js',"highcharts_data_js"));
		wp_enqueue_script("highcharts_cluster_js", "https://code.highcharts.com/modules/marker-clusters.js", array('highcharts_maps_js',"highcharts_data_js","highcharts_proj4_js"));
		wp_enqueue_script("highcharts_col_js", "https://code.highcharts.com/mapdata/countries/co/co-all.js", array('highcharts_maps_js',"highcharts_data_js","highcharts_proj4_js","highcharts_cluster_js"));

	        wp_enqueue_script("ssf_ds4a_ds_common", plugins_url("/ssf_ds4a/js/ssf_ds4a_common.js"), false, "1.0.2");
	 	if(in_array($post_slug, array("dashboard","dashboard-2")))
		      	wp_enqueue_script("ssf_ds4a_ds_general", plugins_url("/ssf_ds4a/js/ssf_ds4a_ds_general.js"), false, "1.0.1");
		if(in_array($post_slug, array("poblacion-vulnerable-beneficiada","vulnerable-population-benefited")))
			wp_enqueue_script("ssf_ds4a_ds_general", plugins_url("/ssf_ds4a/js/ssf_ds4a_ds_vulnerable.js"), false, "1.0.0");

		if(in_array($post_slug, array("evalue-solicitud","evaluate-your-application")))
	                wp_enqueue_script("ssf_ds4a_ds_general", plugins_url("/ssf_ds4a/js/ssf_ds4a_models.js"), false, "1.0.1");

		wp_localize_script( "ssf_ds4a_ds_common", "ssf_ds4a_dictionary", ssf_ds4a_dictionary(get_locale()));
	}

	wp_register_style("ssf_ds4a_css",plugin_dir_url(__FILE__)."css/style.css", false, "1.0.1");
        wp_enqueue_style("ssf_ds4a_css");
}

function ssf_ds4a_ds_general(){
	require "dashboards_templates/ssf_ds4a_ds_general.php";
}

function ssf_ds4a_ds_vulnerable(){
	require "dashboards_templates/ssf_ds4a_ds_vulnerable.php";
}

function ssf_ds4a_ds_models(){
	$dic = ssf_ds4a_dictionary(get_locale());
	ob_start();
	require "dashboards_templates/ssf_ds4a_ds_models.php";
	$output = ob_get_clean();
	foreach ($dic as $clave => $valor) {
		$output = str_replace("{".$clave."}",$valor,$output);
	}
	echo $output;
}

add_action("wp_enqueue_scripts", "load_scripts");

add_shortcode("ssf_ds4a_ds_general", "ssf_ds4a_ds_general");
add_shortcode("ssf_ds4a_ds_vulnerable", "ssf_ds4a_ds_vulnerable");
add_shortcode("ssf_ds4a_ds_models", "ssf_ds4a_ds_models");
