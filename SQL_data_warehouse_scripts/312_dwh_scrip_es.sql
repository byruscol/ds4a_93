/**----------------------------------------DATA CLEANING--------------------------------------*/
/*   Selecciona el m�mero de beneficios liquidados teniendo en cuenta la fecha del reporte    */
/*   Se agrupoa por identificador                                                             */
/*   Se ajusta el tipo de dato de las fecha a timestamp                                       */
/*   Se limpia el c�digo de municipio                                                         */
/*   Se cargan los datos en fosfec."5-312-filtered"                                                       */
/**-------------------------------------------------------------------------------------------*/

--truncate table fosfec."5-312-filtered"
--vacuum full fosfec."5-312-filtered"
INSERT INTO fosfec."5-312-filtered"
(identificador, "Sexo del beneficiario", "Fecha de nacimiento del beneficiario", "Fecha perdida empleo del postulante", "Categoria afiliado", "Fecha de radicacion de solicitud del beneficio", "Fecha de aprobacion del beneficio", "Beneficio economico otorgado", componente, "Fecha de liquidacion del beneficio", "Fecha de suspension del beneficio", "Fecha de reactivacion del beneficio", "Numero de beneficios liquidados", "Codigo DANE del municipio de residencia del beneficiario", "Zona de ubicacion de residencia del beneficiario", "Direccion de residencia del beneficiario", "Grupo etnico a que pertenece el beneficiario", "Caracteristicas de la poblacion", "Factor de vulnerabilidad", "Fecha de reporte", periodo, "Codigo Caja de Compensacion", "Caja de Compensacion", "Fecha Creacion")
select t.* 
from fosfec."5-312" t
	join(
		select f.identificador,  max_ben, min(TO_TIMESTAMP("Fecha de reporte", 'YYYYMMDD')) reporte
		from (
			select identificador,max("Numero de beneficios liquidados"::int) max_ben
			from fosfec."5-312"
			group by identificador
		) s
		join fosfec."5-312" f on f.identificador = s.identificador and s.max_ben = f."Numero de beneficios liquidados"::int
		group by f.identificador, max_ben--, date_trunc('month', TO_TIMESTAMP("Fecha de reporte", 'YYYYMMDD'))
		
	) f on f.identificador = t.identificador and f.max_ben = t."Numero de beneficios liquidados"::int and f.reporte = TO_TIMESTAMP(t."Fecha de reporte", 'YYYYMMDD')

update fosfec."5-312-filtered"
set municipio = lower(dwh.cleaningstring("Codigo DANE del municipio de residencia del beneficiario"))

--truncate table dwh."5-312A_CIRCULAR_COVID"
--truncate table dwh.d_beneficiario cascade
--truncate table dwh.d_caja_compensacion cascade
--truncate table dwh.d_periodo cascade

/**---------------------------------------DATA WAREHOUSE--------------------------------------*/
/*   Selecciona las cajas de compensaci�n por c�digo y las adiciona a la tabla                */
/*   dwh.d_caja_compensacion                                                                  */
/*   dentro del periodo '2015-01-01 00:00:00', '2100-12-31 00:00:00'                          */
/**-------------------------------------------------------------------------------------------*/

INSERT INTO dwh.d_caja_compensacion
(id, caja_name, validfrom, validto, isvalid)
SELECT "Codigo Caja de Compensacion"::int,dwh.cleaningstring("Caja de Compensacion"), '2015-01-01 00:00:00', '2100-12-31 00:00:00', True
FROM fosfec."5-312-filtered"
group by "Codigo Caja de Compensacion",dwh.cleaningstring("Caja de Compensacion")
order by 1;
--vacuum full dwh.d_caja_compensacion


/**---------------------------------------DATA WAREHOUSE--------------------------------------*/
/*   Selecciona periodo las adiciona a la tabla                                               */
/*   dwh.d_periodo                                                                            */
/*   dentro del periodo '2015-01-01 00:00:00', '2100-12-31 00:00:00'                          */
/**-------------------------------------------------------------------------------------------*/

INSERT INTO dwh.d_periodo
(periodo, validfrom, validto, isvalid)
SELECT dwh.cleaningstring(periodo), '2015-01-01 00:00:00', '2100-12-31 00:00:00', True
FROM fosfec."5-312-filtered"
group by dwh.cleaningstring(periodo)
order by 1;
--vacuum full dwh.d_periodo

/**---------------------------------------DATA WAREHOUSE--------------------------------------*/
/*   Selecciona beneficiarios por identificador las adiciona a la tabla                       */
/*   dwh.d_beneficiario                                                                       */
/*   dentro del periodo '2015-01-01 00:00:00', '2100-12-31 00:00:00'                          */
/**-------------------------------------------------------------------------------------------*/

INSERT INTO dwh.d_beneficiario
(genero, fecha_nacimiento, id, categoria_afiliado, ubicacion_residencia, direccion_residencia, grupo_etnico, caracteristica_poblacion, fctor_vulnerabilidad, validfrom, validto, isvalid)
select "Sexo del beneficiario",to_date("Fecha de nacimiento del beneficiario", 'YYYYMMDD')
		,identificador,"Categoria afiliado","Zona de ubicacion de residencia del beneficiario"
		,"Direccion de residencia del beneficiario","Grupo etnico a que pertenece el beneficiario"
		,"Caracteristicas de la poblacion", "Factor de vulnerabilidad"
		, '2015-01-01 00:00:00', '2100-12-31 00:00:00', True
from fosfec."5-312-filtered" 
where identificador in(
	SELECT identificador
	FROM fosfec."5-312-filtered"
	group by identificador
	having count(1) = 1
)

/**---------------------------DATA WAREHOUSE DATA CLEANING------------------------------------*/
/*   Elimina los beneficiarios cuyo id es null y cuya fecha de aprobaci�n es '19000101'       */
/*   Se agrupa por identificador y se incluye en la tabla                                     */
/*   dwh.d_beneficiario                                                                       */
/*   dentro del periodo '2015-01-01 00:00:00', '2100-12-31 00:00:00'                          */
/**-------------------------------------------------------------------------------------------*/

INSERT INTO dwh.d_beneficiario
(genero, fecha_nacimiento, id, categoria_afiliado, ubicacion_residencia, direccion_residencia, grupo_etnico, caracteristica_poblacion, fctor_vulnerabilidad, validfrom, validto, isvalid)
select t."Sexo del beneficiario",to_date(t."Fecha de nacimiento del beneficiario", 'YYYYMMDD')
		,t.identificador,t."Categoria afiliado",t."Zona de ubicacion de residencia del beneficiario"
		,t."Direccion de residencia del beneficiario",t."Grupo etnico a que pertenece el beneficiario"
		,t."Caracteristicas de la poblacion", t."Factor de vulnerabilidad"
		, '2015-01-01 00:00:00', '2100-12-31 00:00:00', True
from (
	select t.*
	from fosfec."5-312-filtered" t
	where t.identificador in (
			select t.identificador
			from fosfec."5-312-filtered" t
				 left join dwh.d_beneficiario b on t.identificador = b.id 
				 left join dwh."5-312A_CIRCULAR_COVID" f on b.beneficiarioid = f.beneficiarioid 
			where f.beneficiarioid is null and t."Fecha de aprobacion del beneficio" <> '19000101'
			group by t.identificador
			having count(1) = 1
		) 
		and t."Fecha de aprobacion del beneficio" <> '19000101'
	)t
	left join dwh.d_beneficiario b on t.identificador = b.id 
	where b.beneficiarioid is null

	--vacuum full dwh.d_beneficiario

/**---------------------------DATA WAREHOUSE DATA CLEANING------------------------------------*/
/*   Recopila los datos del comportamiento del postulante                                     */
/*   en dwh."5-312A_CIRCULAR_COVID" agrupado por identificador                                */
/*   Cuando la cantidad del identificador 1 sea igual a 1                                     */
/**-------------------------------------------------------------------------------------------*/


INSERT INTO dwh."5-312A_CIRCULAR_COVID"
(beneficiarioid, fecha_reporte, fecha_perdida_empleo, fecha_radicacion, fecha_aprobacion, fecha_liquidacion_beneficio, fecha_suspension_beneficio, fecha_reactivacion_beneficio, municipioid, periodoid, cajaid, numero_beneficios_liquidados)
	select b.beneficiarioid, t."Fecha de reporte"::int, t."Fecha perdida empleo del postulante"::int, t."Fecha de radicacion de solicitud del beneficio"::int
		   ,t."Fecha de aprobacion del beneficio"::int, t."Fecha de liquidacion del beneficio"::int
		   ,t."Fecha de suspension del beneficio"::int, t."Fecha de reactivacion del beneficio"::int
		   ,m.municipioid,p.periodoid, c.cajaid, t."Numero de beneficios liquidados"::int
	from fosfec."5-312-filtered" t
		 join dwh.d_beneficiario b on b.id = t.identificador 
		 join dwh.d_caja_compensacion c on c.id = t."Codigo Caja de Compensacion"::int
		 join dwh.d_municipio m on lower(m.municipio) = t.municipio 
		 join dwh.d_periodo p on p.periodo = t.periodo
		 
		 join dwh.d_date d on d."DateKey" = t."Fecha de reporte"::int
		 join dwh.d_date dp on dp."DateKey" = t."Fecha perdida empleo del postulante"::int
		 join dwh.d_date dr on dr."DateKey" = t."Fecha de radicacion de solicitud del beneficio"::int
		 join dwh.d_date da on da."DateKey" = t."Fecha de aprobacion del beneficio"::int
		 join dwh.d_date dl on dl."DateKey" = t."Fecha de liquidacion del beneficio"::int
		 join dwh.d_date ds on ds."DateKey" = t."Fecha de suspension del beneficio"::int
		 join dwh.d_date dra on dra."DateKey" = t."Fecha de reactivacion del beneficio"::int
		 
	where t.identificador in (
		   select t.identificador
			from fosfec."5-312-filtered" t
			 join dwh.d_municipio m on lower(m.municipio) = t.municipio
			 group by t.identificador
			having count(1) = 1
		 )	
--vacuum full dwh."5-312A_CIRCULAR_COVID"

/**---------------------------DATA WAREHOUSE DATA CLEANING------------------------------------*/
/*   Recopila los datos del comportamiento del postulante                                     */
/*   en dwh."5-312A_CIRCULAR_COVID" agrupado por identificador                                */
/*   Cuando el identificador de la tabla de hechos sea nulo y                                 */
/*   la cantidad del identificador 1 sea mayor a 1                                            */
/**-------------------------------------------------------------------------------------------*/

INSERT INTO dwh."5-312A_CIRCULAR_COVID"
(beneficiarioid, fecha_reporte, fecha_perdida_empleo, fecha_radicacion, fecha_aprobacion, fecha_liquidacion_beneficio, fecha_suspension_beneficio, fecha_reactivacion_beneficio, municipioid, periodoid, cajaid, numero_beneficios_liquidados)
	select distinct b.beneficiarioid, t."Fecha de reporte"::int, t."Fecha perdida empleo del postulante"::int, t."Fecha de radicacion de solicitud del beneficio"::int
			   ,t."Fecha de aprobacion del beneficio"::int, t."Fecha de liquidacion del beneficio"::int
			   ,t."Fecha de suspension del beneficio"::int, t."Fecha de reactivacion del beneficio"::int
			   ,m.municipioid,p.periodoid, c.cajaid, t."Numero de beneficios liquidados"::int
		from fosfec."5-312-filtered" t
			 join dwh.d_beneficiario b on b.id = t.identificador 
			 join dwh.d_caja_compensacion c on c.id = t."Codigo Caja de Compensacion"::int
			 join fosfec.cajas_depto cd on trim(cd.caja) = trim(c.caja_name)
			 join dwh.d_municipio m on lower(m.municipio) = t.municipio and lower(m.departamento) = lower(cd.depto)
			 join dwh.d_periodo p on p.periodo = t.periodo
			 
			 join dwh.d_date d on d."DateKey" = t."Fecha de reporte"::int
			 join dwh.d_date dp on dp."DateKey" = t."Fecha perdida empleo del postulante"::int
			 join dwh.d_date dr on dr."DateKey" = t."Fecha de radicacion de solicitud del beneficio"::int
			 join dwh.d_date da on da."DateKey" = t."Fecha de aprobacion del beneficio"::int
			 join dwh.d_date dl on dl."DateKey" = t."Fecha de liquidacion del beneficio"::int
			 join dwh.d_date ds on ds."DateKey" = t."Fecha de suspension del beneficio"::int
			 join dwh.d_date dra on dra."DateKey" = t."Fecha de reactivacion del beneficio"::int
			 --left join dwh."5-312A_CIRCULAR_COVID" f on f.beneficiarioid = b.beneficiarioid and f.fecha_reporte = t."Fecha de reporte"::int
			 where t.identificador in (
			   select t.identificador
				from fosfec."5-312-filtered" t
				 join dwh.d_municipio m on lower(m.municipio) = t.municipio
				 left join dwh.d_beneficiario b on t.identificador = b.id 
				 left join dwh."5-312A_CIRCULAR_COVID" f on b.beneficiarioid = f.beneficiarioid 
				 where f.factid  is null
				 group by t.identificador
				having count(1) > 1
			 )
		and f.factid  is null
--group by t.identificador
--having count(1) > 1
--order by  t.identificador,m.municipio 
--486629
--15324
--460471

/**---------------------------DATA WAREHOUSE DATA CLEANING------------------------------------*/
/*   Recopila los datos del comportamiento del postulante                                     */
/*   en dwh."5-312A_CIRCULAR_COVID" agrupado por identificador                                */
/*   Cuando el la fecha de aprobaci�n del beneficio sea diferente a '19000101'                */
/**-------------------------------------------------------------------------------------------*/
	
INSERT INTO dwh."5-312A_CIRCULAR_COVID"
(beneficiarioid, fecha_reporte, fecha_perdida_empleo, fecha_radicacion, fecha_aprobacion, fecha_liquidacion_beneficio, fecha_suspension_beneficio, fecha_reactivacion_beneficio, municipioid, periodoid, cajaid, numero_beneficios_liquidados)	
select b.beneficiarioid, t."Fecha de reporte"::int, t."Fecha perdida empleo del postulante"::int, t."Fecha de radicacion de solicitud del beneficio"::int
		   ,t."Fecha de aprobacion del beneficio"::int, t."Fecha de liquidacion del beneficio"::int
		   ,t."Fecha de suspension del beneficio"::int, t."Fecha de reactivacion del beneficio"::int
		   ,m.municipioid,p.periodoid, c.cajaid, t."Numero de beneficios liquidados"::int
from (
	select t.*
	from fosfec."5-312-filtered" t
	where t.identificador in (
			select t.identificador
			from fosfec."5-312-filtered" t
				 left join dwh.d_beneficiario b on t.identificador = b.id 
				 left join dwh."5-312A_CIRCULAR_COVID" f on b.beneficiarioid = f.beneficiarioid 
			where f.beneficiarioid is null and t."Fecha de aprobacion del beneficio" <> '19000101'
			group by t.identificador
			having count(1) = 1
		) 
		and t."Fecha de aprobacion del beneficio" <> '19000101'
	)t
	 join dwh.d_beneficiario b on b.id = t.identificador 
	 join dwh.d_caja_compensacion c on c.id = t."Codigo Caja de Compensacion"::int
	 join fosfec.cajas_depto cd on trim(cd.caja) = trim(c.caja_name)
	 join dwh.d_municipio m on lower(m.municipio) = t.municipio and lower(m.departamento) = lower(cd.depto)
	 join dwh.d_periodo p on p.periodo = t.periodo
	 
	 join dwh.d_date d on d."DateKey" = t."Fecha de reporte"::int
	 join dwh.d_date dp on dp."DateKey" = t."Fecha perdida empleo del postulante"::int
	 join dwh.d_date dr on dr."DateKey" = t."Fecha de radicacion de solicitud del beneficio"::int
	 join dwh.d_date da on da."DateKey" = t."Fecha de aprobacion del beneficio"::int
	 join dwh.d_date dl on dl."DateKey" = t."Fecha de liquidacion del beneficio"::int
	 join dwh.d_date ds on ds."DateKey" = t."Fecha de suspension del beneficio"::int
	 join dwh.d_date dra on dra."DateKey" = t."Fecha de reactivacion del beneficio"::int
	 
--14746

/**---------------------------DATA WAREHOUSE DATA CLEANING------------------------------------*/
/*   Recopila los datos del comportamiento del postulante                                     */
/*   en dwh."5-312A_CIRCULAR_COVID" agrupado por identificador                                */
/*   Cuando el identificador del beneficiario sea null, ordenado por identificador            */
/**-------------------------------------------------------------------------------------------*/

INSERT INTO dwh."5-312A_CIRCULAR_COVID"
(beneficiarioid, fecha_reporte, fecha_perdida_empleo, fecha_radicacion, fecha_aprobacion, fecha_liquidacion_beneficio, fecha_suspension_beneficio, fecha_reactivacion_beneficio, municipioid, periodoid, cajaid, numero_beneficios_liquidados)	 

select b.beneficiarioid, t."Fecha de reporte"::int, t."Fecha perdida empleo del postulante"::int, t."Fecha de radicacion de solicitud del beneficio"::int
		   ,t."Fecha de aprobacion del beneficio"::int, t."Fecha de liquidacion del beneficio"::int
		   ,t."Fecha de suspension del beneficio"::int, t."Fecha de reactivacion del beneficio"::int
		   ,m.municipioid,p.periodoid, c.cajaid, t."Numero de beneficios liquidados"::int
from (
	select t.*
	from fosfec."5-312-filtered" t
	where t.identificador in (
			select t.identificador
			from fosfec."5-312-filtered" t
				 left join dwh.d_beneficiario b on t.identificador = b.id 
				 left join dwh."5-312A_CIRCULAR_COVID" f on b.beneficiarioid = f.beneficiarioid 
			where f.beneficiarioid is null 
			group by t.identificador
			having count(1) = 1
		) 
	)t
	 join dwh.d_beneficiario b on b.id = t.identificador 
	 join dwh.d_caja_compensacion c on c.id = t."Codigo Caja de Compensacion"::int
	 join fosfec.cajas_depto cd on trim(cd.caja) = trim(c.caja_name)
	 join dwh.d_municipio m on lower(m.municipio) = t.municipio and lower(m.departamento) = lower(cd.depto)
	 join dwh.d_periodo p on p.periodo = t.periodo
	 join dwh.d_date d on d."DateKey" = t."Fecha de reporte"::int
	 join dwh.d_date dp on dp."DateKey" = t."Fecha perdida empleo del postulante"::int
	 join dwh.d_date dr on dr."DateKey" = t."Fecha de radicacion de solicitud del beneficio"::int
	 join dwh.d_date da on da."DateKey" = t."Fecha de aprobacion del beneficio"::int
	 join dwh.d_date dl on dl."DateKey" = t."Fecha de liquidacion del beneficio"::int
	 join dwh.d_date ds on ds."DateKey" = t."Fecha de suspension del beneficio"::int
	 join dwh.d_date dra on dra."DateKey" = t."Fecha de reactivacion del beneficio"::int
order by t.identificador 	 


/**---------------------------DATA WAREHOUSE DATA CLEANING------------------------------------*/
/* Actualiza los datos contenidos en dwh."5-312A_CIRCULAR_COVID"                              */
/* establece los d�as de aprobaci�n restando dr."Date" a da."Date"                            */
/* permite establecer la fecha de radicaci�n y la fecha de aprobaci�n                         */
/**-------------------------------------------------------------------------------------------*/

update dwh."5-312A_CIRCULAR_COVID" ft
set numero_dias_aprobacion = d.dias_aprobacion
	,beneficio_aprobado = d.aprobado
from (
SELECT factid
	    ,da."Date" fecha_aprobacion 
	    ,dr."Date" fecha_radicacion 
		,case when fecha_aprobacion = 19000101 then null else da."Date" - dr."Date"  end dias_aprobacion
		,case when fecha_aprobacion = 19000101 then False else True end aprobado
FROM dwh."5-312A_CIRCULAR_COVID" f
	 join dwh.d_date dr on dr."DateKey" = f.fecha_radicacion 
	 join dwh.d_date da on da."DateKey" = f.fecha_aprobacion 
)d 
where d.factid = ft.factid ;

--drop view dwh.approvalsbyfilingdate;
create view dwh.approvalsbyfilingdate as
select case when beneficio_aprobado then 'Approved' else 'Not approved' end status,d."Date" filing_date,count(1) total
FROM dwh."5-312A_CIRCULAR_COVID" f
	 join dwh.d_date d on d."DateKey" = f.fecha_radicacion
	 join dwh.d_beneficiario b on b.beneficiarioid = f.beneficiarioid 
where fecha_radicacion > 20191101
group by d."Date" ,beneficio_aprobado
order by  1,2;

