<div class="container-fluid">
	<div class="row">
		<div class="col-md-7">
			<div class="card">
				<div class="card-body">
					<p class="card-text">
						<div class="ds4a_card panel" id="ds_model_form">
							<form id="ds_model_form_object">
							  <div class="form-group row">
								<label class="col-4">{genero}</label> 
								<div class="col-8">
								  <div class="custom-control custom-radio custom-control-inline">
									<input name="genero" id="genero_0" type="radio" required="required" class="custom-control-input" value="HOMBRE"> 
									<label for="genero_0" class="custom-control-label">{hombre}</label>
								  </div>
								  <div class="custom-control custom-radio custom-control-inline">
									<input name="genero" id="genero_1" type="radio" required="required" class="custom-control-input" value="MUJER"> 
									<label for="genero_1" class="custom-control-label">{mujer}</label>
								  </div>
								  <div class="custom-control custom-radio custom-control-inline">
									<input name="genero" id="genero_2" type="radio" required="required" class="custom-control-input" value="INDETERMINADO"> 
									<label for="genero_2" class="custom-control-label">{indeterminado}</label>
								  </div>
								</div>
							  </div>
							  <div class="form-group row">
								<label class="col-4 col-form-label" for="rangoedad">{rangoedad}</label> 
								<div class="col-8">
								  <select id="rangoedad" name="rangoedad" class="custom-select" required="required">
									<option/>
									<option value="1"> {menorde} 16 {anios}</option>
									<option value="2"> {de} 15 {a} 17 {anios}</option>
									<option value="3"> {de} 18 {a} 27 {anios}</option>
									<option value="4"> {de} 28 {a} 37 {anios}</option>
									<option value="5"> {de} 38 {a} 47 {anios}</option>
									<option value="6"> {de} 48 {a} 57 {anios}</option>
									<option value="7"> {de} 58 {a} 67 {anios}</option>
									<option value="8"> {de} 68 {a} 77 {anios}</option>
									<option value="9"> {mayorde} 77 {anios}</option>
								  </select>
								</div>
							  </div>
							  <div class="form-group row">
								<label for="grupo_etnico" class="col-4 col-form-label">{grupo_etnico}</label> 
								<div class="col-8">
								  <select id="grupo_etnico" name="grupo_etnico" class="custom-select" required="required">
									<option/>
									<option value="NINGUNO DE LOS ANTERIORES">{NINGUNO}</option>
									<option value="AFROCOLOMBIANO">{AFROCOLOMBIANO}</option>
									<option value="COMUNIDAD NEGRA">{COMUNIDAD NEGRA}</option>
									<option value="COMUNIDAD RAIZAL">{COMUNIDAD RAIZAL}</option>
									<option value="INDÃ�GENA">{INDIGENA}</option>
									<option value="PALANQUERO">{PALANQUERO}</option>
									<option value="ROOM/GITANO">{ROOM/GITANO}</option>
								  </select>
								</div>
							  </div>
							  <div class="form-group row">
								<label for="region" class="col-4 col-form-label">{region}</label>
								<div class="col-8">
								  <select id="region" name="region" class="custom-select" required="required">
									<option/>
									<option value="Región Caribe">{Región Caribe}</option>
									<option value="Región Centro Sur">{Región Centro Sur}</option>
									<option value="Región Eje Cafetero - Antioquia">{Región Eje Cafetero - Antioquia}</option>
									<option value="Región Centro Oriente">{Región Centro Oriente}</option>
									<option value="Región Pacífico">{Región Pacífico}</option>
									<option value="Región Llano">{Región Llano}</option>
								  </select>
								</div>
							  </div>
							  <div class="form-group row">
								<label class="col-4">{ubicacion_residencia}</label> 
								<div class="col-8">
								  <div class="custom-control custom-radio custom-control-inline">
									<input name="ubicacion_residencia" id="ubicacion_residencia_0" type="radio" required="required" class="custom-control-input" value="RURAL"> 
									<label for="ubicacion_residencia_0" class="custom-control-label">{RURAL}</label>
								  </div>
								  <div class="custom-control custom-radio custom-control-inline">
									<input name="ubicacion_residencia" id="ubicacion_residencia_1" type="radio" required="required" class="custom-control-input" value="URBANA"> 
									<label for="ubicacion_residencia_1" class="custom-control-label">{URBANA}</label>
								  </div>
								</div>
							  </div>
							  <div class="form-group row">
							    <label class="col-4">{factor_vulnerabilidad}</label> 
							    <div class="col-8">
							      <div class="custom-control custom-checkbox custom-control-inline">
							        <input name="factor_vulnerabilidad" id="factor_vulnerabilidad_0" type="checkbox" class="custom-control-input" value="conflicto"> 
							        <label for="factor_vulnerabilidad_0" class="custom-control-label">{conflicto}</label>
							      </div>
							      <div class="custom-control custom-checkbox custom-control-inline">
							        <input name="factor_vulnerabilidad" id="factor_vulnerabilidad_1" type="checkbox" class="custom-control-input" value="desplazamiento"> 
							        <label for="factor_vulnerabilidad_1" class="custom-control-label">{desplazamiento}</label>
							      </div>
							      <div class="custom-control custom-checkbox custom-control-inline">
							        <input name="factor_vulnerabilidad" id="factor_vulnerabilidad_2" type="checkbox" class="custom-control-input" value="discapacidad"> 
							        <label for="factor_vulnerabilidad_2" class="custom-control-label">{discapacidad}</label>
							      </div>
							    </div>
							  </div>
							  <div class="form-group row">
								<label for="caja_name" class="col-4 col-form-label">{caja_name}</label> 
								<div class="col-8">
								  <select id="caja_name" name="caja_name" class="custom-select" required="required">
									<option/>
									<option value="CAFABA">CAFABA</option>
									<option value="CAFAMAZ">CAFAMAZ</option>
									<option value="CAFAM">CAFAM</option>
									<option value="CAFASUR">CAFASUR</option>
									<option value="CAJACOPI Barranquilla">CAJACOPI Barranquilla</option>
									<option value="CAJAMAG">CAJAMAG</option>
									<option value="CAJASAI">CAJASAI</option>
									<option value="CAJASAN">CAJASAN</option>
									<option value="COFREM">COFREM</option>
									<option value="COLSUBSIDIO">COLSUBSIDIO</option>
									<option value="COMBARRANQUILLA">COMBARRANQUILLA</option>
									<option value="COMCAJA">COMCAJA</option>
									<option value="COMFABOY">COMFABOY</option>
									<option value="COMFACA Comfamiliar Caqueta">COMFACA Comfamiliar Caqueta</option>
									<option value="COMFACASANARE">COMFACASANARE</option>
									<option value="COMFACAUCA">COMFACAUCA</option>
									<option value="COMFACESAR">COMFACESAR</option>
									<option value="COMFACHOCO">COMFACHOCO</option>
									<option value="COMFACOR">COMFACOR</option>
									<option value="COMFACUNDI">COMFACUNDI</option>
									<option value="COMFAGUAJIRA">COMFAGUAJIRA</option>
									<option value="COMFAMA">COMFAMA</option>
									<option value="COMFAMILIAR Atlántico">COMFAMILIAR Atlántico</option>
									<option value="COMFAMILIAR Caldas">COMFAMILIAR Caldas</option>
									<option value="COMFAMILIAR CAMACOL">COMFAMILIAR CAMACOL</option>
									<option value="COMFAMILIAR de Cartagena y Bolivar ">COMFAMILIAR de Cartagena y Bolivar </option>
									<option value="COMFAMILIAR Huila">COMFAMILIAR Huila</option>
									<option value="COMFAMILIAR Nariño">COMFAMILIAR Nariño</option>
									<option value="COMFAMILIAR Putumayo">COMFAMILIAR Putumayo</option>
									<option value="COMFAMILIAR Risaralda">COMFAMILIAR Risaralda</option>
									<option value="COMFANDI">COMFANDI</option>
									<option value="COMFANORTE">COMFANORTE</option>
									<option value="COMFAORIENTE">COMFAORIENTE</option>
									<option value="COMFASUCRE">COMFASUCRE</option>
									<option value="COMFATOLIMA">COMFATOLIMA</option>
									<option value="COMFENALCO Antioquia">COMFENALCO Antioquia</option>
									<option value="COMFENALCO Cartagena">COMFENALCO Cartagena</option>
									<option value="COMFENALCO Quindio">COMFENALCO Quindio</option>
									<option value="COMFENALCO Santander">COMFENALCO Santander</option>
									<option value="COMFENALCO Tolima">COMFENALCO Tolima</option>
									<option value="COMFENALCO Valle del Cauca">COMFENALCO Valle del Cauca</option>
									<option value="COMFIAR Arauca">COMFIAR Arauca</option>
									<option value="COMPENSAR">COMPENSAR</option>
								  </select>
								</div>
							  </div>
							  <div class="form-group row">
								<label class="col-4">{categoria_afiliado}</label>
								<div class="col-8">
								  <div class="custom-control custom-radio custom-control-inline">
									<input name="categoria_afiliado" id="categoria_afiliado_0" type="radio" required="required" class="custom-control-input" value="A">
									<label for="categoria_afiliado_0" class="custom-control-label">A</label>
								  </div>
								  <div class="custom-control custom-radio custom-control-inline">
									<input name="categoria_afiliado" id="categoria_afiliado_1" type="radio" required="required" class="custom-control-input" value="B">
									<label for="categoria_afiliado_1" class="custom-control-label">B</label>
								  </div>
								  <div class="custom-control custom-radio custom-control-inline">
									<input name="categoria_afiliado" id="categoria_afiliado_2" type="radio" required="required" class="custom-control-input" value="C">
									<label for="categoria_afiliado_2" class="custom-control-label">C</label>
								  </div>
								  <div class="custom-control custom-radio custom-control-inline">
									<input name="categoria_afiliado" id="categoria_afiliado_3" type="radio" required="required" class="custom-control-input" value="D">
									<label for="categoria_afiliado_3" class="custom-control-label">D</label>
								  </div>
								</div>
							  </div>
							  <div class="form-group row">
								<div class="offset-4 col-8">
								  <button name="submit" type="submit" class="btn btn-primary" id="ds_model_form_submit">{Submit}</button>
								</div>
							  </div>
							</form>
						</div>
					</p>
				</div>
			</div>
		</div>
		<div class="col-md-5">
			<div class="card">
				<div class="card-body">
					<p class="card-text">
						<div class="ds4a_card" id="ds_model_legend">
							<h5 class="card-title">{modeltitle}</h5>
							{ds_model_legend}
						</div>
						<div class="ds4a_card">
							<div class="text-info" role="status" id="ds_model_loading">
  								<span class="sr-only">Loading...</span>
							</div>
							<div class="alert " role="alert" id="ds_model_result"></div>
						</div>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
