<div class="container-fluid">
	<div class="row">
		<div class="col-md-3">
			<div class="card">
				<div class="card-body">
					<div class="ds4a_card" id="card_general_1">
						<div class="icon" id="card_general_1_icon"></div>
						<div class="data" id="card_general_1_data">
							<div class="metric" id="card_general_1_metric"></div>
							<div class="title" id="card_general_1_title"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="card">
                                <div class="card-body">
					<div class="ds4a_card" id="card_general_2">
						<div class="icon" id="card_general_2_icon"></div>
						<div class="data" id="card_general_2_data">
							<div class="metric" id="card_general_2_metric"></div>
							<div class="title" id="card_general_2_title"></div>
						</div>
					</div>
                                </div>
                        </div>
		</div>
		<div class="col-md-3">
			<div class="card">
                               	<div class="card-body">
					<div class="ds4a_card" id="card_general_3">
						<div class="icon" id="card_general_3_icon"></div>
						<div class="data" id="card_general_3_data">
							<div class="metric" id="card_general_3_metric"></div>
							<div class="title" id="card_general_3_title"></div>
						</div>
					</div>
                                </div>
                        </div>
		</div>
		<div class="col-md-3">
			<div class="card">
                                <div class="card-body">
					<div class="ds4a_card" id="card_general_4"> 
						<div class="icon" id="card_general_4_icon"></div>
						<div class="data" id="card_general_4_data">
							<div class="metric" id="card_general_4_metric"></div>
							<div class="title" id="card_general_4_title"></div>
						</div>
					</div>
                                </div>
                        </div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="card">
				<div class="card-body">
					<h5 class="card-title" id="ds_general_map_title"></h5>
					<p class="card-text">
						<div class="ds4a_card panel" id="ds_general_map"></div>
					</p>
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="row">
                                <div class="col-md-6">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title" id="ds_general_genre_title"></h5>
							<p class="card-text">
								<div class="ds4a_card panel" id="ds_general_genre"></div>
							</p>
						</div>
					</div>
                                </div>
				<div class="col-md-6">
							<div class="card">
                                                <div class="card-body">
                                                        <h5 class="card-title" id="ds_general_cat_title"></h5>
                                                        <p class="card-text">
                                                        	<div class="ds4a_card panel" id="ds_general_cat"></div>
                                                        </p>
                                                </div>
                                        </div>
                                </div>
                        </div>
			<div class="row">
				<div class="col-md-12">
					<div class="card">
                                		<div class="card-body">
                                        		<h5 class="card-title" id="ds_general_timeseries_title"></h5>
                                			<p class="card-text">
                                        			<div class="ds4a_card panel" id="ds_general_timeseries"></div>
                                			</p>
                                		</div>
                        		</div>
				</div>
			</div>
		</div>
	</div>
</div>
