<div class="container-fluid">
	<div class="row">
		<div class="col-md-3">
			<div class="card">
				<div class="card-body">
					<div class="ds4a_card" id="card_vulnerable_1">
						<div class="icon" id="card_vulnerable_1_icon"></div>
						<div class="data" id="card_vulnerable_1_data">
							<div class="metric" id="card_vulnerable_1_metric"></div>
							<div class="title" id="card_vulnerable_1_title"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="card">
                                <div class="card-body">
					<div class="ds4a_card" id="card_vulnerable_2">
						<div class="icon" id="card_vulnerable_2_icon"></div>
						<div class="data" id="card_vulnerable_2_data">
							<div class="metric" id="card_vulnerable_2_metric"></div>
							<div class="title" id="card_vulnerable_2_title"></div>
						</div>
					</div>
                                </div>
                        </div>
		</div>
		<div class="col-md-3">
			<div class="card">
                               	<div class="card-body">
					<div class="ds4a_card" id="card_vulnerable_3">
						<div class="icon" id="card_vulnerable_3_icon"></div>
						<div class="data" id="card_vulnerable_3_data">
							<div class="metric" id="card_vulnerable_3_metric"></div>
							<div class="title" id="card_vulnerable_3_title"></div>
						</div>
					</div>
                                </div>
                        </div>
		</div>
		<div class="col-md-3">
			<div class="card">
                                <div class="card-body">
					<div class="ds4a_card" id="card_vulnerable_4"> 
						<div class="icon" id="card_vulnerable_4_icon"></div>
						<div class="data" id="card_vulnerable_4_data">
							<div class="metric" id="card_vulnerable_4_metric"></div>
							<div class="title" id="card_vulnerable_4_title"></div>
						</div>
					</div>
                                </div>
                        </div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="card">
				<div class="card-body">
					<h5 class="card-title" id="ds_vulnerable_map_title"></h5>
					<p class="card-text">
						<div class="ds4a_card panel" id="ds_vulnerable_map"></div>
					</p>
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="row">
                                <div class="col-md-6">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title" id="ds_vulnerable_pie_title"></h5>
							<p class="card-text">
								<div class="ds4a_card panel" id="ds_vulnerable_pie"></div>
							</p>
						</div>
					</div>
                                </div>
				<div class="col-md-6">
							<div class="card">
                                                <div class="card-body">
                                                        <h5 class="card-title" id="ds_vulnerable_timeseries_title"></h5>
                                                        <p class="card-text">
                                                        	<div class="ds4a_card panel" id="ds_vulnerable_timeseries"></div>
                                                        </p>
                                                </div>
                                        </div>
                                </div>
                        </div>
			<div class="row">
				<div class="col-md-12">
					<div class="card">
                                		<div class="card-body">
                                        		<h5 class="card-title" id="ds_vulnerable_table_title"></h5>
                                			<p class="card-text">
                                        			<div class="ds4a_card panel" id="ds_vulnerable_table_layer">
									<table id="ds_vulnerable_table" class="display compact" style="width:100%">
                                                                                <thead>
                                                                                        <tr>
                                                                                                <th id="caja_name"></th>
                                                                                                <th id="sum_discapacidad"></th>
                                                                                                <th id="sum_desplazamiento"></th>
                                                                                                <th id="sum_conflicto"></th>
                                                                                         </tr>
                                                                                </thead>
                                                                        </table>
								</div>
                                			</p>
                                		</div>
                        		</div>
				</div>
			</div>
		</div>
	</div>
</div>
