function number_format(num)
{
    var num_parts = num.toString().split(".");
    num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return num_parts.join(".");
}
function ssf_ds4a_dashbords(){
	this.host = 'http://www.covidssf.com/wp-json/';
}
ssf_ds4a_dashbords.prototype.setcardtitle = function(id) {
	jQuery( "#"+id+"_title" ).html(ssf_ds4a_dictionary[id+"_title"]);
}
ssf_ds4a_dashbords.prototype.model1 = function(url, properties) {
	jQuery( "#"+properties.id).html("");
	jQuery( "#"+properties.id).removeClass("alert-success");
	jQuery( "#"+properties.id).removeClass("alert-warning");
	jQuery( "#ds_model_loading").addClass("spinner-border");
        jQuery.ajax({
			url: this.host + url
			,method: "POST"
  			,data: properties.data
		})
                .done(function(data) {
			jQuery( "#ds_model_loading").removeClass("spinner-border");
			jQuery( "#"+properties.id).addClass(((data.data.modelresult == 1)?"alert-success":"alert-warning"));
			jQuery( "#"+properties.id).html(((data.data.modelresult == 1)?ssf_ds4a_dictionary['requestapproved']:ssf_ds4a_dictionary['requestnotapproved']));
                });
};

ssf_ds4a_dashbords.prototype.cards = function(url, properties) {
	jQuery.ajax(this.host + url)
  		.done(function(data) {
    			for(i in properties){
				jQuery( "#"+properties[i].id+"_icon" ).addClass(properties[i].icon);
				jQuery( "#"+properties[i].id+"_metric" ).html(number_format(data.data[0][properties[i].key]));
				jQuery( "#"+properties[i].id+"_title" ).html(ssf_ds4a_dictionary[properties[i].id+"_title"]);
			}
  		});
};

ssf_ds4a_dashbords.prototype.rendertable = function(id, url, columns) {
	this.setcardtitle(id);
	for(i in columns){
		jQuery( "#"+columns[i].data).html(ssf_ds4a_dictionary[columns[i].data]);
	}
 	jQuery('#'+id).DataTable( {
        	 "ajax": this.host + url,
                "columns": columns
        });
};

ssf_ds4a_dashbords.prototype.renderpie = function(id, url, properties) {
	this.setcardtitle(id);
        Highcharts.getJSON(
                this.host + url,
                function (data) {
			dataserie = [];
			series = [];
			for (i in data.data){
                                dataserie.push({"name":ssf_ds4a_dictionary[data.data[i][properties.name]], "y":parseInt(data.data[i][properties.y]),"sliced": true,"selected": true})
                        }
                        series.push({"data":dataserie});
			Highcharts.chart(id, {
			    chart: {
			        plotBackgroundColor: null,
			        plotBorderWidth: null,
			        plotShadow: false,
			        type: 'pie'
			    },
			    title: {
			        text: ''
			    },
			    tooltip: {
			        pointFormat: '<b>{point.name}</b>: {point.y:.1f}({point.percentage:.1f} %)'
			    },
			    accessibility: {
			        point: {
			            valueSuffix: '%'
			        }
			    },
			    plotOptions: {
			        pie: {
			            allowPointSelect: true,
			            cursor: 'pointer',
			            dataLabels: {
			                enabled: true,
			                format: '<b>{point.name}</b>'
			            }
			        }
			    },
			    series: series
			});
		}
	);
}

ssf_ds4a_dashbords.prototype.rendercolmap = function(id, url, properties) {
	this.setcardtitle(id);
	Highcharts.getJSON(
	    	this.host + url,
		function (data) {
			colombiageodata = Highcharts.maps["countries/co/co-all"];
			basicdata = [];
			for(i in data.summarybydepto.data){
				for(j in colombiageodata.features){
					name = "";
					switch(colombiageodata.features[j].properties.name){
						case "Bogota": name = "Bogotá D.C.";break;
						default: name = colombiageodata.features[j].properties.name;
					}
					if(data.summarybydepto.data[i].departamento == name){
						total = parseInt(data.summarybydepto.data[i][properties.total]);
						metric1 = parseInt(data.summarybydepto.data[i][properties.metric1]);
						metric2 = parseInt(data.summarybydepto.data[i][properties.metric2]);
						metric3 = parseInt(data.summarybydepto.data[i][properties.metric3]);
						basicdata.push([colombiageodata.features[j].properties["hc-key"],total,metric1,metric2,metric3]);
					}
				}
			}
			for(i in data.summarybymunicipio.data){
				data.summarybymunicipio.data[i].lat = parseFloat(data.summarybymunicipio.data[i].lat);
				data.summarybymunicipio.data[i].lon = parseFloat(data.summarybymunicipio.data[i].lon);
			}
			Highcharts.mapChart(id, {
			  chart: {
				  map: 'countries/co/co-all'
			  },

			  title: {
				  text: ''
			  },

			  mapNavigation: {
				  enabled: true,
				  buttonOptions: {
					  verticalAlign: 'bottom'
				  }
			  },
			colorAxis: [
				{min:0},
				{
			        	minColor: '#ff7a7a',
			        	maxColor: '#e60000'
			    	}
			],
			tooltip: {
			            headerFormat: '',
				    useHTML: true,
			            pointFormat: '<b>{point.municipio}</b><br>'
						 +ssf_ds4a_dictionary[properties.metric1]+': {point.'+ properties.metric1 +':.2f}<br/>'
						 +ssf_ds4a_dictionary[properties.metric2]+': {point.'+ properties.metric2 +':.2f}<br/>'
						 +ssf_ds4a_dictionary[properties.metric3]+': {point.'+ properties.metric3 +':.2f}<br/>'
						 +'<hr/>Total: {point.'+ properties.total  +':.2f}'
			        },
			plotOptions: {
		            mappoint: {
		                cluster: {
		                    enabled: true,
		                    allowOverlap: false,
			                    animation: {
			                        duration: 450
			                    },
			                    layoutAlgorithm: {
			                        type: 'grid',
	        				gridSize: 70
	    				    },
			                    zones: [{
				                        from: 1,
				                        to: 4,
				                        marker: {
				                            radius: 5
				                        }
				                    }, {
				                        from: 5,
				                        to: 9,
				                        marker: {
				                            radius: 8
				                        }
				                    }, {
				                        from: 10,
				                        to: 15,
				                        marker: {
				                            radius: 10
				                        }
				                    }, {
				                        from: 16,
				                        to: 20,
				                        marker: {
				                            radius: 15
				                        }
				                    }, {
				                        from: 21,
				                        to: 1000,
				                        marker: {
				                            radius: 18
				                        }
				                    }]
				                }
				            }
				        },
			  series: [{
					  data: basicdata,
					  joinBy: ['hc-key', 'id'],
					  keys: ['id', 'value', properties.metric1, properties.metric2, properties.metric3],
					  name: 'Cantidad total de solicitudes',
					  tooltip: {
						headerFormat: '',
						pointFormatter: function () {
							//for(i in this)
							//	console.log(i +"->" + this[i])
							return '<b> '+this.name+' </b>'
	                					+'<br/>'+ssf_ds4a_dictionary[properties.metric1]+': '+ number_format(this[properties.metric1])
	                					+'<br/>'+ssf_ds4a_dictionary[properties.metric2]+': ' + number_format(this[properties.metric2])
	                					+'<br/>'+ssf_ds4a_dictionary[properties.metric3]+': ' + number_format(this[properties.metric3])
								+'<hr/>Total: ' + number_format(this.value)
						}
					  },
					  dataLabels: {
						  enabled: true,
						  format: '{point.name}'
					  }
				  }, {
	    				colorAxis: 1,
					type: 'mappoint',
	    				enableMouseTracking: true,
	    				colorKey: 'clusterPointsAmount',
	    				name: 'Cantidad total de solicitudes',
	    				data: data.summarybymunicipio.data
			        }
			]
		});
	}
   );
}

ssf_ds4a_dashbords.prototype.rendercolumn = function(id, url, properties) {
	this.setcardtitle(id);
	Highcharts.getJSON(
    		this.host + url,
		function (data) {
			var categories = [];
			var series = [];
			var seriename = "";
			for (i in data.data){
				if(categories.indexOf(data.data[i][properties.categories]) === -1) {
    					categories.push(data.data[i][properties.categories]);
				}
				if(seriename != data.data[i][properties.series]){
					if(seriename != ""){
						series.push({"name":ssf_ds4a_dictionary[seriename], "data":dataserie, stack:properties.series});
					}
					seriename = data.data[i][properties.series];
					dataserie = [];
				}
				dataserie.push(parseFloat(data.data[i][properties.value]))
			}
			series.push({"name":ssf_ds4a_dictionary[seriename], "data":dataserie, stack:properties.series});
			Highcharts.chart(id, {
			    chart: {
			        type: 'column'
			    },

			    title: {
			        text: ''
			    },

			    xAxis: {
			        categories: categories
			    },

			    yAxis: {
			        allowDecimals: false,
			        min: 0,
			        title: {
			            text: ssf_ds4a_dictionary['yAxisTitle']
			        }
			    },

			    tooltip: {
			        formatter: function () {
			            return '<b>' + this.x + '</b><br/>' +
			                this.series.name + ': ' + this.y + '<br/>' +
			                'Total: ' + this.point.stackTotal;
			        }
			    },

			    plotOptions: {
			        column: {
			            stacking: 'normal'
			        }
			    },

			    series: series
			});
	});
}

ssf_ds4a_dashbords.prototype.renderbar = function(id, url, properties) {
    this.setcardtitle(id);
	Highcharts.getJSON(
		this.host + url,
		function (data) {
			var categories = [];
			var series = [];
			var seriename = "";
			for (i in data.data){
				if(categories.indexOf(data.data[i][properties.categories]) === -1) {
					categories.push(data.data[i][properties.categories]);
				}
				if(seriename != data.data[i][properties.series]){
					if(seriename != ""){
						series.push({"name":ssf_ds4a_dictionary[seriename], "data":dataserie});
					}
		                    	seriename = data.data[i][properties.series];
					dataserie = [];
				}
				dataserie.push(parseFloat(data.data[i][properties.value]))
			}
			series.push({"name":ssf_ds4a_dictionary[seriename], "data":dataserie, stack:properties.series});
			Highcharts.chart(id, {
			    chart: {
					type: 'bar'
				},
				title: {
					text: ''
				},
				xAxis: {
				    categories: categories
				},
				yAxis: {
				    allowDecimals: false,
					min: 0,
					title: {
						text: ssf_ds4a_dictionary['yAxisTitle']
					}
				},
           			tooltip: {
					formatter: function () {
					return '<b>' + this.x + '</b><br/>' +
							this.series.name + ': ' + this.y + '<br/>';
							}
				},
				plotOptions: {
					column: {
						stacking: 'normal'
					}
				},
				series: series
			});
		});
	}

ssf_ds4a_dashbords.prototype.rendertimeseries = function(id, url, properties) {
	this.setcardtitle(id);
	Highcharts.getJSON(
			this.host + url,
			function (data) {
			var series = []
			seriename = "";
			dataserie = [];
			for (i in data.data){
				if(seriename != data.data[i].status){
					seriename = data.data[i].status;
					dataserie = [];
					if(seriename != ""){
						series.push({"name":ssf_ds4a_dictionary[seriename], "data":dataserie});
					}
				}
				var parts = data.data[i].filing_date.split("-");
				epoch = Date.UTC(parts[0], parts[1]-1, parts[2]);
				dataserie.push([epoch,Number(data.data[i].total)]);
			}

			Highcharts.chart(id, {
				chart: {
					type: 'line'
				},
				title: {
					text: ""
				},
				xAxis: {
					type: 'datetime'
				},
				yAxis: {
					title: {
						text: properties.yAxisTitle
					}
				},
				tooltip: {
						shared: true,
						crosshairs: true
					},
				plotOptions: {
					series: {
								cursor: 'pointer',
								point: {
									events: {
											click: function (e) {
												hs.htmlExpand(null, {
														pageOrigin: {
															x: e.pageX || e.clientX,
															y: e.pageY || e.clientY
														},
														headingText: this.series.name,
														maincontentText: Highcharts.dateFormat('%A, %b %e, %Y', this.x) + ':<br/> ' +
														this.y,
														width: 200
												});
											}
									}
								},
								  marker: {
								 lineWidth: 1
								}
						}
				},
				series: series
			});
		}
	);
}
