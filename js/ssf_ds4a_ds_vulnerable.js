jQuery(document).ready(function() {
	var vulnerable_dashboard = new ssf_ds4a_dashbords();
	vulnerable_dashboard.cards('DS4A_93/v1/api/summary/summarybyvulnerablepopulation'
				,[
					{"id":"card_vulnerable_1", "units":"", "icon":"fa fa-wheelchair","key":"sum_discapacidad"},
					{"id":"card_vulnerable_2", "units":"", "icon":"fa fa-user-times","key":"sum_desplazamiento"},
					{"id":"card_vulnerable_3", "units":"H", "icon":"fa fa-ambulance","key":"sum_conflicto"},
					{"id":"card_vulnerable_4", "units":"", "icon":"fa fa-percent","key":"vulnerable_percentage"}
				 ])
	vulnerable_dashboard.rendertable('ds_vulnerable_table'
					, 'DS4A_93/v1/api/summary/summarybyvulnerablebycaja'
					,[
				           	 { "data": "caja_name" },
					         { "data": "sum_discapacidad" },
					         { "data": "sum_desplazamiento" },
					         { "data": "sum_conflicto" }
				         ] );

	vulnerable_dashboard.rendercolmap('ds_vulnerable_map', 'DS4A_93/v1/api/summaryvulnerablebydeptomunicipio', {"total":"total","metric1":"sum_discapacidad","metric2":"sum_desplazamiento","metric3":"sum_conflicto"})

	vulnerable_dashboard.renderpie('ds_vulnerable_pie'
					, 'DS4A_93/v1/api/summary/summarybyvulnerablesuspended'
					,{ "name": "vulnerable","y":"count" }
	 );
	vulnerable_dashboard.rendertimeseries('ds_vulnerable_timeseries'
					   ,'DS4A_93/v1/api/summary/summaryvulnerablepopulationbyfilingdate'
					   ,{"yAxisTitle":'Total'}
	);

//console.log(ssf_ds4a_dictionary)
});
