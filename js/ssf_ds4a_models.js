jQuery(document).ready(function() {
	jQuery("#ds_model_form_object").submit(function(e){
        	e.preventDefault();
		var models_dashboard = new ssf_ds4a_dashbords();
		var formdata = jQuery('#ds_model_form_object').serializeArray();
		var data = {};
		for(i in formdata){
			if( formdata[i].name == 'factor_vulnerabilidad')
				data[formdata[i].value] = 1;
			else
                        	data[formdata[i].name] = (formdata[i].name=='rangoedad')?parseInt(formdata[i].value):formdata[i].value;
                }

		if (!data.hasOwnProperty("conflicto"))
			data["conflicto"] = 0;
		if (!data.hasOwnProperty("desplazamiento"))
			data["desplazamiento"] = 0;
		if (!data.hasOwnProperty("discapacidad"))
			data["discapacidad"] = 0;

		models_dashboard.model1('DS4A_93/v1/api/model',{"data":data,id:"ds_model_result"})
    	});
});

