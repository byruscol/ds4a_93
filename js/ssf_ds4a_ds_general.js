jQuery(document).ready(function() {
	var general_dashboard = new ssf_ds4a_dashbords();
	general_dashboard.cards('DS4A_93/v1/api/summary/summarydata'
				,[
					{"id":"card_general_1", "units":"", "icon":"fa fa-thumbs-up","key":"approved"},
					{"id":"card_general_2", "units":"", "icon":"fa fa-thumbs-down","key":"notapproved"},
					{"id":"card_general_3", "units":"H", "icon":"fa fa-clock-o","key":"averageapprovaldays"},
					{"id":"card_general_4", "units":"", "icon":"fa fa-exclamation-triangle","key":"suspended"}
				 ])
	general_dashboard.rendercolmap('ds_general_map', 'DS4A_93/v1/api/summarydatabydeptomunicipio', {"total":"total","metric1":"approved","metric2":"notapproved","metric3":"avgdays"})

	general_dashboard.rendercolumn('ds_general_genre'
					, 'DS4A_93/v1/api/summary/summarydatabygender'
					,{ "categories": "gender","series": "status","value":"total","yAxisTitle":'numberofbenefits' }
	 );
	general_dashboard.renderbar('ds_general_cat'
                                        , 'DS4A_93/v1/api/summary/summarydatabycategory'
                                        ,{ "categories": "category","series": "status","value":"total","yAxisTitle":'numberofbenefits' }
         );
	general_dashboard.rendertimeseries('ds_general_timeseries'
					   ,'DS4A_93/v1/api/summary/approvalsbyfilingdate'
					   ,{"yAxisTitle":'Total'}
	);
//console.log(ssf_ds4a_dictionary)
});
